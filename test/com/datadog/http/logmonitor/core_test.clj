(ns com.datadog.http.logmonitor.core-test
  (:require [clojure.test :refer :all]
            [net.cgrand.xforms :as x]
            [com.datadog.http.logmonitor.core :refer :all]))

(deftest parsing-xform-test
  (testing
      "Tests if the parsing transducer correctly parses log lines"
    (is (= [{:remote-host "\"10.0.0.2\"" :rfc931 "\"-\"" :auth-user "\"apache\"" :date 1549573860
             :request "\"GET /api/user HTTP/1.0\"" :status "200" :bytes "1234" :section "/api"}
            {:remote-host "\"10.0.0.3\"" :rfc931 "\"-\"" :auth-user "\"apache\"" :date 1549573861
             :request "\"GET /report HTTP/1.0\"" :status "200" :bytes "1234" :section "/report"}
            {:remote-host "\"10.0.0.4\"" :rfc931 "\"-\"" :auth-user "\"apache\"" :date 1549573861
             :request "\"GET / HTTP/1.0\"" :status "200" :bytes "1234" :section "/"}]
           (into [] parsing-xform
                 ["\"remotehost\",\"rfc931\",\"authuser\",\"date\",\"request\",\"status\",\"bytes\"",
                  "\"10.0.0.2\",\"-\",\"apache\",1549573860,\"GET /api/user HTTP/1.0\",200,1234"
                  "\"10.0.0.3\",\"-\",\"apache\",1549573861,\"GET /report HTTP/1.0\",200,1234"
                  "\"10.0.0.4\",\"-\",\"apache\",1549573861,\"GET / HTTP/1.0\",200,1234"])))))

(deftest flushing-test
  (let [sample (into [] (map (partial hash-map :date))
                     (range 4))
        queue (array-deque sample)
        flush-queue (flush-policy 4)]
    (testing
        "Tests that the head of the queue is popped when the window length is
         exceeded by 1"
        (let [queue (doto queue
                      (.add {:date 5})
                      (flush-queue))]
          (is (= {:date 1} (.peekFirst queue)))))
    (testing
        "Tests that the head of the queue is popped repeatedly until
         the resulting queue is within the desired limit,
         especially when the next enqueue operation has a timestamp
         far ahead of the tail of the queue"
        (let [queue (doto queue
                      (.add {:date 8})
                      (flush-queue))]
          (is (= {:date 5} (.peekFirst queue)))
          (is (= 2 (count queue)))))))

(deftest summarize-fn-test
  (let [sample (into [] (map #(assoc {} :section (mod % 2) :date %))
                     (range 5))
        stats-map {:freq-by-section (x/into-by-key {} :section x/count)
                   :window (comp (map :date)
                                 (x/transjuxt [x/min x/max]))
                   :total-count x/count}
        queue (array-deque sample)
        summarize (summarize-fn 8 stats-map)]
    (testing
        "Tests that summary stats are not emitted when range of timestamps is within limit"
        (let [_ (dotimes [n 4]
                  (summarize queue {:date (+ n 5) :section (mod (+ n 5) 2)}))]
          (is (= false (empty? queue)))
          (is (= {:date 0 :section 0} (.peekFirst queue)))
          (is (= {:date 8 :section 0} (.peekLast queue)))))
    (testing
        "Tests that summary stats are emitted when the next enqueue operation is determined to exceed
         the specified time window. Tests that queue is cleared and stats emitted before enqueue operation is performed"
        (let [std-out (with-out-str
                        (summarize queue {:date 9 :section 1}))]
          (is (= false (empty? queue)))
          (is (= {:date 9 :section 1} (.peekFirst queue)))
          (is (= 1 (count queue)))
          (is (= "{:freq-by-section {0 5, 1 4}, :window [0 8], :total-count 9}\n" std-out))))
    (testing
        "Tests that summary stats are emitted when the next enqueue operation has a timestamp greater than 1 window length
         ahead of the queue tail."
        (let [std-out (with-out-str
                        (summarize queue {:date 18 :section 1}))]
          (is (= false (empty? queue)))
          (is (= {:date 18 :section 1} (.peekFirst queue)))
          (is (= 1 (count queue)))
          (is (= "{:freq-by-section {1 1}, :window [9 9], :total-count 1}\n" std-out))))))


(def sample-parsed-input
  [{:remote-host "\"10.0.0.3\"", :rfc931 "\"-\"", :auth-user "\"apache\"", :date 0,
    :request "\"GET /api/user HTTP/1.0\"", :status "200", :bytes "1234", :section 0}
   {:remote-host "\"10.0.0.1\"", :rfc931 "\"-\"", :auth-user "\"apache\"", :date 1,
    :request "\"GET /api/user HTTP/1.0\"", :status "200", :bytes "1234", :section 1}
   {:remote-host "\"10.0.0.0\"", :rfc931 "\"-\"", :auth-user "\"apache\"", :date 2,
    :request "\"GET /api/user HTTP/1.0\"", :status "200", :bytes "1234", :section 0}
   {:remote-host "\"10.0.0.0\"", :rfc931 "\"-\"", :auth-user "\"apache\"", :date 3,
    :request "\"GET /api/user HTTP/1.0\"", :status "200", :bytes "1234", :section 1}
   {:remote-host "\"10.0.0.1\"", :rfc931 "\"-\"", :auth-user "\"apache\"", :date 4,
    :request "\"GET /api/user HTTP/1.0\"", :status "200", :bytes "1234", :section 0}
   {:remote-host "\"10.0.0.2\"", :rfc931 "\"-\"", :auth-user "\"apache\"", :date 5,
    :request "\"GET /api/user HTTP/1.0\"", :status "200", :bytes "1234", :section 1}
   {:remote-host "\"10.0.0.5\"", :rfc931 "\"-\"", :auth-user "\"apache\"", :date 6,
    :request "\"GET /api/user HTTP/1.0\"", :status "200", :bytes "1234", :section 0}
   {:remote-host "\"10.0.0.3\"", :rfc931 "\"-\"", :auth-user "\"apache\"", :date 6,
    :request "\"GET /api/user HTTP/1.0\"", :status "200", :bytes "1234", :section 1}
   {:remote-host "\"10.0.0.4\"", :rfc931 "\"-\"", :auth-user "\"apache\"", :date 6,
    :request "\"GET /api/user HTTP/1.0\"", :status "200", :bytes "1234", :section 0}
   {:remote-host "\"10.0.0.0\"", :rfc931 "\"-\"", :auth-user "\"apache\"", :date 6,
    :request "\"GET /api/user HTTP/1.0\"", :status "200", :bytes "1234", :section 1}
   {:remote-host "\"10.0.0.1\"", :rfc931 "\"-\"", :auth-user "\"apache\"", :date 6,
    :request "\"GET /api/user HTTP/1.0\"", :status "200", :bytes "1234", :section 0}
   {:remote-host "\"10.0.0.3\"", :rfc931 "\"-\"", :auth-user "\"apache\"", :date 7,
    :request "\"GET /api/user HTTP/1.0\"", :status "200", :bytes "1234", :section 1}
   {:remote-host "\"10.0.0.5\"", :rfc931 "\"-\"", :auth-user "\"apache\"", :date 8,
    :request "\"GET /api/user HTTP/1.0\"", :status "200", :bytes "1234", :section 0}
   {:remote-host "\"10.0.0.5\"", :rfc931 "\"-\"", :auth-user "\"apache\"", :date 8,
    :request "\"GET /api/user HTTP/1.0\"", :status "200", :bytes "1234", :section 1}
   {:remote-host "\"10.0.0.2\"", :rfc931 "\"-\"", :auth-user "\"apache\"", :date 8,
    :request "\"GET /api/user HTTP/1.0\"", :status "200", :bytes "1234", :section 0}
   {:remote-host "\"10.0.0.5\"", :rfc931 "\"-\"", :auth-user "\"apache\"", :date 9,
    :request "\"GET /api/user HTTP/1.0\"", :status "200", :bytes "1234", :section 1}
   {:remote-host "\"10.0.0.4\"", :rfc931 "\"-\"", :auth-user "\"apache\"", :date 12,
    :request "\"GET /api/user HTTP/1.0\"", :status "200", :bytes "1234", :section 0}
   {:remote-host "\"10.0.0.1\"", :rfc931 "\"-\"", :auth-user "\"apache\"", :date 13,
    :request "\"GET /api/user HTTP/1.0\"", :status "200", :bytes "1234", :section 1}])

(deftest log-reducer-test
  (let [conf {:stats-interval-seconds 5
              :threshold-exceeded-seconds 2
              :threshold 3}
        log-reducer (log-reducer-fn conf)
        test-fn (fn [parsed-input]
                  (into [] (x/reduce log-reducer)
                        parsed-input))]
    (testing
        "Tests that no alerts and no summary stats are printed within first 5
         log seconds since the summary stats interval is set to 5"
        (let [out (with-out-str (test-fn (take 6 sample-parsed-input)))
              out (clojure.string/split out #"\n")]
          (is (= out
                 [""]))))
    (testing
        "Tests that summary stats are printed for the first time when log time
         first reaches 6 seconds (> 5)"
        (let [out (with-out-str (test-fn (take 7 sample-parsed-input)))
              out (clojure.string/split out #"\n")]
          (is (= out
                 [(format "{:freq-by-section {0 3, 1 3}, :window (1970-01-01T00:00:%02dZ 1970-01-01T00:00:%02dZ), :total-count 6}"
                          0 5)]))))
    (testing
        "Tests that the alert generated message is printed at the correct time
         and that the alert recovered message is also printed at the correct time.
         Also tests that summary stats are printed every 5 log seconds (2 times within 13 seconds)"
        (let [out (with-out-str (test-fn sample-parsed-input))
              out (clojure.string/split out #"\n")]
          (is (= out
                 ["{:freq-by-section {0 3, 1 3}, :window (1970-01-01T00:00:00Z 1970-01-01T00:00:05Z), :total-count 6}"
                  "HIGH TRAFFIC ALERT GENERATED - request-rate = 3.500000, hits = 7, triggered at 1970-01-01T00:00:06Z"
                  "HIGH TRAFFIC ALERT RECOVERED - request-rate = 2.500000, hits = 5, recovered at 1970-01-01T00:00:09Z"
                  "{:freq-by-section {0 5, 1 5}, :window (1970-01-01T00:00:06Z 1970-01-01T00:00:09Z), :total-count 10}"]))))))
