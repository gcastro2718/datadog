(ns com.datadog.http.logmonitor.core
  (:gen-class)
  (:require [net.cgrand.xforms :as x]
            [net.cgrand.xforms.io :as xio]
            [clojure.tools.cli :as cli])
  (:import java.util.ArrayDeque
           (java.net ServerSocket)))

(def parsing-xform
  "Transducer to parse lines in input stream"
  (comp (drop 1)
        (map #(clojure.string/split % #","))
        (map (partial zipmap [:remote-host :rfc931 :auth-user :date :request :status :bytes]))
        (map #(update % :date (fn [x]
                                (Integer/parseInt x))))
        (map (fn [{:keys [request] :as line}]
               (let [[_ section] (re-find #"(/[^/\s]*)[/\s]+" request)]
                 (assoc line :section section))))))

(defn array-deque
  "Wrapper over java.util.ArrayDeque constructor"
  ([]
   (java.util.ArrayDeque.))
  ([coll]
   (java.util.ArrayDeque. coll)))


(defn flush-policy
  "Returns a function that takes a queue(deque) and enforces that
  the timestamps of the first and last element in the queue are no more
  than window-length apart. Closes over provided window-length."
  [window-length]
  (fn [queue]
      (while (> (- (:date (.peekLast queue))
                   (:date (.peekFirst queue)))
                window-length)
        (.pollFirst queue))))


(defn flush-and-update-fn
  "Returns a function that adds a new element to the queue and enforces a flush policy on the queue.
  State is updated and alerts are emitted depending on the resulting state."
  [window-length threshold]
  (let [flush-queue (flush-policy window-length)
        window-length (double window-length)
        threshold (double threshold)]
    (fn [{:keys [curr-state queue]} x]
      (let [_ (.add queue x)
            _ (flush-queue queue)
            size (count queue)
            request-rate (/ size window-length)
            state-transition (cond
                                (and (> request-rate threshold) (= curr-state :normal)) :alert
                                (and (<= request-rate threshold) (= curr-state :alert)) :normal
                                :else nil)
            _ (when-not (nil? state-transition)
                (let [curr-time (-> queue
                                    (.peekLast)
                                    :date
                                    (java.time.Instant/ofEpochSecond)
                                    str)]
                  (case curr-state
                    :normal (println (format "HIGH TRAFFIC ALERT GENERATED - request-rate = %f, hits = %d, triggered at %s"
                                          request-rate
                                          size
                                          curr-time))
                    :alert  (println (format "HIGH TRAFFIC ALERT RECOVERED - request-rate = %f, hits = %d, recovered at %s"
                                             request-rate
                                             size
                                             curr-time)))))
              curr-state (or state-transition curr-state)]
          {:curr-state curr-state
           :queue queue}))))

(def summary-xforms
  "A map describing a set of named computations over a sequence of parsed logs. Implemented as a map of transducers"
  {:freq-by-section (x/into-by-key {} :section x/count)
   :window (comp (map :date)
                     (x/transjuxt [x/min x/max])
                     (map (partial map (comp str
                                             #(java.time.Instant/ofEpochSecond %)))))
   :total-count x/count})

(defn summarize-fn
  "Returns a function that adds an element to the stats buffer.
  When buffer is full, summary stats are computed and emitted.
  Queue is then cleared before the current elemetn is added."
  [interval computation-map]
    (fn [buffer-queue x]
      (do
        (when (and (not (.isEmpty buffer-queue))
                   (> (- (:date x)
                         (:date (.peekFirst buffer-queue)))
                      interval))
          (println (into {} (x/transjuxt computation-map)
                         buffer-queue))
          (.clear buffer-queue))
        (.add buffer-queue x)
        buffer-queue)))

(defn log-reducer-fn
  "Hydrates and returns a reducing function over parsed log lines
  to enable logging of summary stats and alerting"
  [{interval :stats-interval-seconds
    window-length :threshold-exceeded-seconds
    threshold :threshold
    :or {interval 10 window-length 120 threshold 10}}]
   (let [flush-and-update (flush-and-update-fn window-length threshold)
         summarize (summarize-fn interval summary-xforms)]
     (fn
       ([]
        {:stats (array-deque)
         :alerting {:curr-state :normal
                    :queue (array-deque)}})
       ([acc]acc)
       ([acc x]
        (-> acc
            (update :alerting flush-and-update x)
            (update :stats summarize x))))))


(defn usage
  [options-summary]
  (clojure.string/join \newline
                       ["Terminal based HTTP Log Monitor processes comma delimited HTTP log stream from file, std-in, or local port."
                        ""
                        "The log format should match the sample csv in the resources directory. The first line is assumed to be the header."
                        ""
                        "Either log-file, std-in, or port must be specified as the input source."
                        "If multiple input sources are specified, the order of preference is log-file followed by std-in and finally port."
                        ""
                        "Options:"
                        options-summary
                        ""]))

(defn -main
  "Terminal based HTTP Log Monitor.  Processes a stream of log lines and reports summary stats at a specified interval.
  Prints an alert when requests rate exceeds a specified threshold over some specified window of item."
  [& args]
  (let [cli-options [[nil "--log-file " "Process log lines from file located at given path."]
                     [nil "--std-in" "Process log lines from STDIN."]
                     [nil "--port " "Process log lines from socket on local port."]
                     [nil "--threshold " "Requests per second threshold. Defaults to 10."]
                     [nil "--threshold-exceeded-seconds " "Alert when requests per second exceeds threshold on average for greater than specified seconds. Defaults to 120."]
                     [nil "--stats-interval-seconds " "Print summary stats every specified number of seconds. Defaults to 10."]
                     [nil "--help" "Display this information."]]
        {:keys [options errors summary]} (cli/parse-opts args cli-options)
        _ (cond
            (or (:help options)
                (not (some options '(:log-file :std-in :port))))
              (do
                (println (usage summary))
                (System/exit 0))
            errors
              (do
                (println errors)
                (System/exit 1)))
        {:keys [threshold threshold-exceeded-seconds stats-interval-seconds]} options
        options (merge options
                 (when threshold
                   {:threshold (Integer/parseInt threshold)})
                 (when threshold-exceeded-seconds
                   {:threshold-exceeded-seconds (Integer/parseInt threshold-exceeded-seconds)})
                 (when stats-interval-seconds
                   {:stats-interval (Integer/parseInt stats-interval-seconds)}))
        log-reducer (log-reducer-fn options)
        source (cond
                 (:log-file options) (:log-file options)
                 (:std-in options) *in*
                 :else
                 (let [port (Integer/parseInt (:port options))
                       server-sock (ServerSocket. port)]
                   (.accept server-sock)))]
    (into [] (comp parsing-xform
                   (x/reduce log-reducer))
          (xio/lines-in source))))
