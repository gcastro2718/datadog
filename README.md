# Datadog HTTP Log Monitor Assignment
![Assignment Prompt](https://i.ibb.co/x3CsyGw/image.png)

## Prerequisite
Java 8 (To execute the program)  
[leinegen](https://leiningen.org/) (To compile from source)

## Installation

Installation is not required to run the program as a precompiled standalone jar is located in the target directory.  
To compile from source, install [leinegen](https://leiningen.org/) and execute the following from within the directory: 
```
    $ lein uberjar
```

## Usage
### From a File
```
    $ java -jar target/uberjar/com.datadog.http.logmonitor-0.1.0-SNAPSHOT-standalone.jar --log-file [file path]
```
### From STDIN
```
    $ cat [source] | java -jar target/uberjar/com.datadog.http.logmonitor-0.1.0-SNAPSHOT-standalone.jar --std-in
```
### From a Socket on a Local Port
```
    $ java -jar target/uberjar/com.datadog.http.logmonitor-0.1.0-SNAPSHOT-standalone.jar --port [port] &
    $ cat [source] | nc localhost [port]
```

## Options
```
Terminal based HTTP Log Monitor processes comma delimited HTTP log stream from file, std-in, or local port.

The log format should match the sample csv in the resources directory.
The first line is assumed to be the header.

Either log-file, std-in, or port must be specified as the input source.
If multiple input sources are specified, the order of preference is log-file followed by std-in and finally port.

Options:
      --log-file                     Process log lines from file located at given path.
      --std-in                       Process log lines from STDIN.
      --port                         Process log lines from socket on local port.
      --threshold                    Requests per second threshold. Defaults to 10.
      --threshold-exceeded-seconds   Alert when requests per second exceeds threshold on average for greater than specified seconds. Defaults to 120.
      --stats-interval-seconds       Print summary stats every specified number of seconds. Defaults to 10.
      --help                         Display this information.
```

## Example Output  

```
{:freq-by-section {/api 66, /report 33}, :window (2019-02-07T21:10:59Z 2019-02-07T21:11:10Z), :total-count 99}
{:freq-by-section {/api 61, /report 30}, :window (2019-02-07T21:11:10Z 2019-02-07T21:11:21Z), :total-count 91}
{:freq-by-section {/api 71, /report 36}, :window (2019-02-07T21:11:21Z 2019-02-07T21:11:32Z), :total-count 107}
{:freq-by-section {/api 60, /report 30}, :window (2019-02-07T21:11:32Z 2019-02-07T21:11:43Z), :total-count 90}
{:freq-by-section {/api 68, /report 33}, :window (2019-02-07T21:11:42Z 2019-02-07T21:11:54Z), :total-count 101}
{:freq-by-section {/api 120, /report 33}, :window (2019-02-07T21:11:53Z 2019-02-07T21:12:05Z), :total-count 153}
{:freq-by-section {/api 154, /report 33}, :window (2019-02-07T21:12:04Z 2019-02-07T21:12:16Z), :total-count 187}
{:freq-by-section {/api 170, /report 33}, :window (2019-02-07T21:12:15Z 2019-02-07T21:12:27Z), :total-count 203}
HIGH TRAFFIC ALERT GENERATED - request-rate = 10.008333, hits = 1201, triggered at 2019-02-07T21:12:36Z
{:freq-by-section {/api 171, /report 33}, :window (2019-02-07T21:12:26Z 2019-02-07T21:12:38Z), :total-count 204}
{:freq-by-section {/api 155, /report 33}, :window (2019-02-07T21:12:38Z 2019-02-07T21:12:49Z), :total-count 188}
{:freq-by-section {/api 164, /report 33}, :window (2019-02-07T21:12:48Z 2019-02-07T21:13:00Z), :total-count 197}
{:freq-by-section {/api 23, /report 11}, :window (2019-02-07T21:12:59Z 2019-02-07T21:13:11Z), :total-count 34}
{:freq-by-section {/api 22, /report 11}, :window (2019-02-07T21:13:11Z 2019-02-07T21:13:22Z), :total-count 33}
{:freq-by-section {/api 23, /report 12}, :window (2019-02-07T21:13:22Z 2019-02-07T21:13:33Z), :total-count 35}
{:freq-by-section {/api 22, /report 11}, :window (2019-02-07T21:13:33Z 2019-02-07T21:13:44Z), :total-count 33}
{:freq-by-section {/api 24, /report 12}, :window (2019-02-07T21:13:44Z 2019-02-07T21:13:56Z), :total-count 36}
HIGH TRAFFIC ALERT RECOVERED - request-rate = 9.800000, hits = 1176, recovered at 2019-02-07T21:14:06Z
{:freq-by-section {/api 22, /report 11}, :window (2019-02-07T21:13:57Z 2019-02-07T21:14:07Z), :total-count 33}
{:freq-by-section {/api 22, /report 11}, :window (2019-02-07T21:14:07Z 2019-02-07T21:14:18Z), :total-count 33}
{:freq-by-section {/api 24, /report 12}, :window (2019-02-07T21:14:18Z 2019-02-07T21:14:30Z), :total-count 36}
{:freq-by-section {/api 22, /report 11}, :window (2019-02-07T21:14:31Z 2019-02-07T21:14:41Z), :total-count 33}
{:freq-by-section {/api 20, /report 10}, :window (2019-02-07T21:14:42Z 2019-02-07T21:14:51Z), :total-count 30}
{:freq-by-section {/api 24, /report 12}, :window (2019-02-07T21:14:51Z 2019-02-07T21:15:03Z), :total-count 36}
{:freq-by-section {/api 24, /report 12}, :window (2019-02-07T21:15:04Z 2019-02-07T21:15:15Z), :total-count 36}
{:freq-by-section {/api 22, /report 11}, :window (2019-02-07T21:15:16Z 2019-02-07T21:15:26Z), :total-count 33}
{:freq-by-section {/api 182, /report 24}, :window (2019-02-07T21:15:27Z 2019-02-07T21:15:37Z), :total-count 206}
{:freq-by-section {/api 274, /report 33}, :window (2019-02-07T21:15:36Z 2019-02-07T21:15:48Z), :total-count 307}
{:freq-by-section {/api 277, /report 33}, :window (2019-02-07T21:15:47Z 2019-02-07T21:15:59Z), :total-count 310}
HIGH TRAFFIC ALERT GENERATED - request-rate = 10.008333, hits = 1201, triggered at 2019-02-07T21:16:03Z
{:freq-by-section {/api 274, /report 33}, :window (2019-02-07T21:15:58Z 2019-02-07T21:16:10Z), :total-count 307}
{:freq-by-section {/api 291, /report 33}, :window (2019-02-07T21:16:09Z 2019-02-07T21:16:21Z), :total-count 324}
{:freq-by-section {/api 260, /report 33}, :window (2019-02-07T21:16:21Z 2019-02-07T21:16:32Z), :total-count 293}
{:freq-by-section {/api 298, /report 36}, :window (2019-02-07T21:16:31Z 2019-02-07T21:16:43Z), :total-count 334}
{:freq-by-section {/api 254, /report 30}, :window (2019-02-07T21:16:43Z 2019-02-07T21:16:54Z), :total-count 284}
{:freq-by-section {/api 152, /report 24}, :window (2019-02-07T21:16:53Z 2019-02-07T21:17:04Z), :total-count 176}
{:freq-by-section {/api 12, /report 11}, :window (2019-02-07T21:17:05Z 2019-02-07T21:17:16Z), :total-count 23}
{:freq-by-section {/report 11, /api 10}, :window (2019-02-07T21:17:17Z 2019-02-07T21:17:27Z), :total-count 21}
{:freq-by-section {/api 10, /report 10}, :window (2019-02-07T21:17:28Z 2019-02-07T21:17:37Z), :total-count 20}
{:freq-by-section {/api 13, /report 12}, :window (2019-02-07T21:17:38Z 2019-02-07T21:17:49Z), :total-count 25}
{:freq-by-section {/report 11, /api 10}, :window (2019-02-07T21:17:50Z 2019-02-07T21:18:00Z), :total-count 21}
{:freq-by-section {/api 12, /report 11}, :window (2019-02-07T21:18:01Z 2019-02-07T21:18:11Z), :total-count 23}
{:freq-by-section {/report 11, /api 10}, :window (2019-02-07T21:18:12Z 2019-02-07T21:18:22Z), :total-count 21}
HIGH TRAFFIC ALERT RECOVERED - request-rate = 9.766667, hits = 1172, recovered at 2019-02-07T21:18:25Z
{:freq-by-section {/api 11, /report 10}, :window (2019-02-07T21:18:23Z 2019-02-07T21:18:33Z), :total-count 21}
{:freq-by-section {/report 11, /api 11}, :window (2019-02-07T21:18:34Z 2019-02-07T21:18:44Z), :total-count 22}
{:freq-by-section {/report 11, /api 10}, :window (2019-02-07T21:18:45Z 2019-02-07T21:18:55Z), :total-count 21}
```
...

### Testing
```
    $ lein test
```
### Known Issues

#### Out of Order Timestamps
The timestamps in the file are not in monotonic order although most of the timestamps are in non-decreasing order   
and the out of order timestamps are generally within a few seconds of the correct placement.  

Alerting and stats reporting utilize sliding windows implemented over queues (java.util.ArrayDeque)  
that maintain the invariant that the difference between the head and tail timestamps is within  
the specified alerting threshold (or stats reporting interval) and that for all timestamps t in the queue,  
queue.head.timestamp <= t <= queue.tail.timestamp.

If timesetamps are out of order, this technically violates the invariants but if we can assume that timestamps will only be mildly out of order,
the program's behavior should still meet any practical expectations for alerting.  If the requirements are strict, we may be able to mitigate the impact of
out of order timestamps using one or more priority queues as a buffer and dropping severely out of order log-lines.

#### Log Time and Time Gaps
Because this program processes logs from any time period, alerts are issued in "log time" not system time.  The windows of time used for alerting (--threshold-exceeded-seconds) 
and stats reporting (--stats-interval-seconds) are also in "log time".

The effect of this choice is that time effectively "stops" in between log arrivals.
The consequence of this is that if we are in the state of alert, we will remain in the state of alert indefinitely until new log lines arrive.
Similarly, no new summary stats will be reported until new log lines arrive. 

We might be able to simulate the passage of time by maintaining an internal clock relative to the first timestamp in the log line, but I feel that the challenge
of maintaing a coherent timeline under these conditions will require more thought especially without monotonicity guarantees.

### Potential Improvements
* Current console output is barebones.  A tabular format similar to the unix tool "top"  would be much more aesthetic. It would update in place and perhaps even highlight alerts.
* If the throughput requirement exceeds what can be accomplished on a single-core, then we can explore a multi-threaded implementation where we distribute computation across threads
and aggregate appropriately.  Maintaining the coherence of timestamps across threads could be challenging especially with the possibility of out of order timestamps which is why I prefer
to cross this bridge only when we have exhausted the performance of a single threaded implementation.
* Alerts and statistics may yield additional insights into the behavior of a system even after they have been emitted.  It could be useful to persist the alerts and stats to a file
in lieu of (or in addition to) printing to screen.  Writing to file also has the added benefit of generally being a faster operation than printing to screen so we may be able to improve
throughput as well.
* We can easily add additonal summary statistics because the statistics are implemented over a map of functions which is easily extensible.  In particular, we might want to add "group-by"
statistics to aggregate by host, request type, or other request routes in addition to the top-level section.  Do larger requests occur during peaks or valleys? How are the requests distributed over the hosts?  It might be interesting to see how the type of requests and number of bytes requested correlate with the volume of traffic over time.  We can also consider maintaing a buffer of previously computed statistics in order to display rates of change (or absolute change) over time.
* The program can currently read from a file, stdin, or a local port.  It appears possible to read logs from a port on a remote machine without extensively modifying the current
implementation.  This opens up the possibility of a barebones monitoring system over remote machines (or containers) although I will admit I am not very familiar with the security
challenges involved with implementing such a feature beyond a toy example.

## License

Copyright © 2019

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
